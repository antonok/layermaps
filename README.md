# layermaps.c

Submodule repository containing a layermap file for Anton Lazarev's custom keyboard design.

This repository is meant to be included in [the keyboard's firmware](https://gitlab.com/antonok/kb).

The layermaps itself are written in a human-friendly C-compiler-compatible format, but can be edited using [kb-layout-manager](https://gitlab.com/antonok/kb-layout-manager).
